import type { PropType } from "vue"

interface dataType {
  [key: string]: string
  readonly id: string
}
interface columnType {
  prop: string
  label?: string
  width?: string | number
  align?: string
}
interface paginationType {
  total: number
  pageSize: number
  currentPage: number
  pageCount: number
  pagerCount?: number
  pageSizes?: number[]
  background?: boolean
  disabled?: boolean
}
interface radioOptionsType {
  columnName: string
  model: any | null
  valueKey: string
  valueLabel: string
}

const props = {
  // 数据源
  data: {
    type: Array as PropType<dataType[]>,
    required: true,
    default: () => []
  },
  // 数据列
  columns: {
    type: Array as PropType<columnType[] | string[]>,
    required: true,
    default: () => []
  },
  // 分页器配置
  pagination: {
    type: [Object, Boolean] as PropType<paginationType | boolean>,
    required: true,
    default: () => ({
      total: 0,
      page: 1,
      pageSize: 10,
      pageSizes: [20, 40, 60],
      pagerCount: 10,
      background: true,
      disabled: false
    })
  },
  // 数据是否加载成功
  isLoading: {
    type: Boolean as PropType<boolean>,
    default: false
  },
  // 是否带有纵向边框
  border: {
    type: Boolean as PropType<boolean>,
    default: true
  },
  // 是否要高亮当前行
  highlight: {
    type: Boolean as PropType<boolean>,
    default: true
  },
  // 是否使用斑马纹表格
  stripe: {
    type: Boolean as PropType<boolean>,
    default: false
  },
  // 是否可选择（单选）需高亮且不可多选
  isRadio: {
    type: Boolean as PropType<boolean>,
    default: false
  },
  // 单选配置
  radioOptions: {
    type: Object as PropType<radioOptionsType>,
    default: () => ({ model: null, valueKey: "", valueLabel: "", columnName: "选择" })
  },
  // 内容过长是否显示tooltip
  overToolTip: {
    type: Boolean as PropType<boolean>,
    default: true
  },
  // 分页器布局
  layout: {
    type: String as PropType<string>,
    default: "total, sizes, prev, pager, next, jumper"
  },
  size: {
    type: String,
    default: "large"
  },
  showHeader: {
    type: Boolean as PropType<boolean>,
    default: true
  },
  rowKey: {
    type: Boolean as PropType<boolean>,
    default: true
  },
  expandRowKeys: {
    type: Array as PropType<dataType[]>,
    default: () => []
  },
  // 是否展示表格索引
  isIndex: {
    type: Boolean as PropType<boolean>,
    default: false
  },
  // 索引label
  labelIndex: {
    type: String as PropType<string>,
    default: "序号"
  },
  // 是否展示多选框选择多行数据
  isSelection: {
    type: Boolean as PropType<boolean>,
    default: false
  },
  tableHeight: {
    type: [String, Number] as PropType<string | number>,
    default: 0
  },
  // 是否多个规则排序
  multipleSort: {
    type: Boolean as PropType<boolean>,
    default: false
  },
  defaultSort: {
    type: Array as PropType<unknown>,
    default: () => []
  },
  emptyText: String as PropType<string>,
  showSummary: Boolean as PropType<boolean>,
  rowClassName: {
    type: [String, Function] as PropType<string | ((o?: object, idx?: number) => string)>,
    default: ""
  },
  // 仅对 type=selection 的列有效，类型为 Function，Function 的返回值用来决定这一行的 CheckBox 是否可以勾选
  selectable: {
    type: Function as PropType<() => boolean>,
    default: () => true
  },
  refresh: {
    type: Function as PropType<() => void>,
    default: () => void 0
  },
  pageCurrentChange: {
    type: Function as PropType<() => void>,
    default: () => void 0
  },
  currentChange: {
    type: Function as PropType<(value: number) => void>,
    default: () => void 0
  },
  handleSizeChange: {
    type: Function as PropType<() => void>,
    default: () => void 0
  }
} as const

export default props
