import { createRouter, createWebHashHistory, createWebHistory } from "vue-router"
import type { RouteRecordRaw, RouteMeta } from "vue-router"
import NProgress from "nprogress" // progress bar

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const pageModules = import.meta.glob("../views/**/page.json", { eager: true, import: "default" })
const compModules = import.meta.glob("../views/**/index.vue")

export const routes: RouteRecordRaw[] = Object.entries(pageModules).map(([pagePath, meta]) => {
  const path = pagePath.replace("../views", "").replace("/page.json", "") || "/"
  const name = path.split("/").filter(Boolean).join("-") || "index"
  const compPath = pagePath.replace("page.json", "index.vue")
  return {
    path,
    name,
    component: compModules[compPath],
    meta: meta as RouteMeta,
    redirect: "",
    children: [] as RouteRecordRaw[]
  }
})

// export const routes: RouteRecordRaw[] = [
//   {
//     path: "/",
//     name: "Home",
//     redirect: "/index"
//   },
//   {
//     path: "/index",
//     name: "Index",
//     component: () => import("@/layouts/index.vue"),
//     meta: {
//       title: "首页"
//     },
//     children: []
//   },
//   // Layout之外的路由
//   dashboard,
//   glow
// ]

const router = createRouter({
  // process.env.BASE_URL
  history: createWebHashHistory(),
  routes
})

export default router
