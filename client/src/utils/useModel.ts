import { computed } from "vue"

type TEmit = (event: string, ...args: any[]) => void
export function useModel<T extends Record<string, any>, K extends TEmit>(
  props: T,
  propName: string,
  emit: K
) {
  return computed({
    get() {
      return new Proxy(props[propName], {
        set(target, key, value) {
          emit("update:" + propName, { ...target, [key]: value })
          return Reflect.set(target, key, value)
        }
      })
    },
    set(value) {
      emit("update:" + propName, value)
    }
  })
}
