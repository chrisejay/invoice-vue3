package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"server/utils/Time"
	"server/utils/Users"
)

type List struct {
	Name    string `json:"name"`
	Date    string `json:"date"`
	Address string `json:"address"`
}

type Response struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Data    Data   `json:"data"`
}

type Data struct {
	Total   int    `json:"total"`
	Records []List `json:"records"`
}

type Request struct {
	Current  int `json:"current"`
	PageSize int `json:"pageSize"`
}

var list = func() []List {
	arr := []List{
		{Name: "Alice", Date: "2022-06-12", Address: "London"},
		{Name: "Bob", Date: "2022-12-12", Address: "Manchester"},
		{Name: "Charlie", Date: "2023-04-12", Address: "Berlin"},
	}
	for i := 0; i < 100; i++ {
		var listItem = List{Name: Users.GetUserNameList()[i], Date: Time.GetNowTime(), Address: Users.GetUserAddressList()[i]}
		arr = append(arr, listItem)
	}
	return arr
}()

var allowHeaders = "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization"

func setHeaders(w http.ResponseWriter) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Request-Method", "GET,POST,OPTIONS,PUT,DELETE")
	w.Header().Set("Access-Control-Allow-Headers", allowHeaders)
	w.Header().Set("Content-Type", "application/json")
}

func getPageData(list []List, current int, pageSize int) Data {
	if current < 1 {
		current = 1
	}
	if pageSize < 1 {
		pageSize = 10
	}
	start := (current - 1) * pageSize
	end := start + pageSize
	if start > len(list) {
		start = len(list)
	}
	if end > len(list) {
		end = len(list)
	}
	return Data{
		Total:   len(list),
		Records: list[start:end],
	}
}

func GiveList(w http.ResponseWriter, r *http.Request) {
	var req Request
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		//http.Error(w, err.Error(), http.StatusBadRequest)
		fmt.Printf("err: %v \n", err)
		return
	}
	pagedData := getPageData(list, req.Current, req.PageSize)
	response := Response{Data: pagedData, Code: 200, Message: "ok"}
	data, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		fmt.Printf("err: %v \n", err)
		return
	}
	setHeaders(w)
	w.Write(data)
}
