const request = (params: any) => ({ data: { token: "" } })
export function login(data: API.LoginParams) {
  return request<BaseResponse<API.LoginResult>>(
    {
      url: "login",
      method: "post",
      data
    },
    {
      isGetDataDirectly: false
    }
  )
}
