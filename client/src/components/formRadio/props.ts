import type { PropType } from "vue"
import type { filterOptionsType } from "../app-filter/props"

const props = {
  val: {
    type: [String, Number] as PropType<string | number>,
    default: ""
  },
  options: {
    type: Array as PropType<filterOptionsType[]>,
    default: (): filterOptionsType[] => []
  },
  label: {
    type: String as PropType<string>,
    default: "id"
  },
  oKey: {
    type: String as PropType<string>,
    default: "id"
  },
  title: {
    type: String as PropType<string>,
    default: "title"
  },
  disabled: {
    type: Boolean as PropType<boolean>,
    default: false
  },
  size: {
    type: String as PropType<string>,
    default: ""
  },
  border: {
    type: Boolean as PropType<boolean>,
    default: true
  },
  labelMarginRight: {
    type: String as PropType<string>,
    default: "20px"
  },
  clearable: {
    type: Boolean as PropType<boolean>,
    default: false
  },
  changeValue: {
    type: Function as PropType<(value: number | string) => void>,
    default: (value: string | number) => void 0
  },
  clearValue: {
    type: Function as PropType<() => void>
  }
}

export default props
