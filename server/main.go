package main

import (
	"context"
	"fmt"
	"net/http"
	"server/pkg/BigFileUpload"
	http2 "server/pkg/http"
)

func main() {
	server := &http.Server{
		Addr: ":8000",
	}
	http.HandleFunc("/getList", http2.GiveList)
	fmt.Println("Server started")
	if err := server.ListenAndServe(); err != nil {
		fmt.Println(err)
	}
	BigFileUpload.BigFileUpload()
	defer server.Shutdown(context.Background())
}
