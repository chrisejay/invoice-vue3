import { createApp } from "vue"
// import ElementPlus from "element-plus"
import reportWebVitals from "./reportWebVitals"
import router from "./router"
import store from "./store"
// import ElementPlus from "element-plus"
// import "element-plus/dist/index.css"

import App from "./App.vue"

// import "element-plus/dist/index.css"
import "./style.css"

const app = createApp(App)

// app.use(ElementPlus)
app.use(router)
app.use(store)
app.mount("#app")

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals(console.log);
