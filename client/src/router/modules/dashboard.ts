import type { RouteRecordRaw } from "vue-router"

const dashboard: RouteRecordRaw = {
  path: "/dashboard",
  name: "dashboard",
  meta: {
    title: "首页",
    permiss: "1"
  },
  component: () => import(/* webpackChunkName: "dashboard" */ "@/views/dashboard/index.vue"),
  children: []
}

export default dashboard
