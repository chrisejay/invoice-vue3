import type { RouteRecordRaw } from "vue-router"

const glow: RouteRecordRaw = {
  path: "/glow",
  name: "glow",
  meta: {
    title: "glow",
    permiss: "1"
  },
  component: () => import(/* webpackChunkName: "glow" */ "@/views/glow/index.vue"),
  children: []
}

export default glow
