// 高级扩展类型
export type Record<K extends string | number | symbol, T> = {
  [P in K]: T
}

// 多层对象扩展类型
export type DeepRecord<Obj extends Record<string, any>> = {
  [Key in keyof Obj]: Obj[Key] extends Record<string, any>
    ? DeepRecord<Obj[Key]> & Record<string, any>
    : Obj[Key]
} & Record<string, any>
