package Time

import (
	"strconv"
	"time"
)

func GetNowTime() string {
	year := strconv.Itoa(time.Now().Year())
	month := strconv.Itoa(int(time.Now().Month()))
	day := strconv.Itoa(time.Now().Day())
	hour := strconv.Itoa(time.Now().Hour())
	minute := strconv.Itoa(time.Now().Minute())
	second := strconv.Itoa(time.Now().Second())
	nanoSec := strconv.Itoa(time.Now().Nanosecond())
	return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second + "." + nanoSec
}
