import type { PropType } from "vue"

const props = {
  size: {
    type: String as PropType<string>,
    default: ""
  },
  disabled: {
    type: Boolean as PropType<boolean>,
    default: false
  },
  placeholder: {
    type: String as PropType<string>,
    default: "请选择"
  },
  clearable: {
    type: Boolean as PropType<boolean>,
    default: true
  },
  // 是否可选择任意一项
  checkStrictly: {
    type: Boolean as PropType<boolean>,
    default: false
  },
  // 是否仅显示最后一级
  showAllLevels: {
    type: Boolean as PropType<boolean>,
    default: true
  },
  //键名、键值，子键、父键
  label: {
    type: String as PropType<string>,
    default: "label"
  },
  value: {
    type: String as PropType<string>,
    default: "value"
  },
  children: {
    type: String as PropType<string>,
    default: "children"
  },
  parent: {
    type: String as PropType<string>,
    default: "parent"
  },
  originWhich: {
    type: String as PropType<string>,
    default: "regionData" // regionData/provinceAndCityData/diyData
  },
  defaultAddress: {
    type: String as PropType<string>,
    default: ""
  },
  diyData: {
    // 传入选项
    type: Array as PropType<string[]>,
    default: () => []
  },
  // 查看特定某地区
  address: {
    type: Array as PropType<string[]>,
    default: () => []
  },
  editAllLevels: {
    // 是否传递全部选项(即multiple)
    type: [Boolean, String] as PropType<boolean | string>,
    default: true
  },
  // 值类型
  valType: {
    type: String as PropType<string>,
    default: "default" // default/array
  }
}

export default props
