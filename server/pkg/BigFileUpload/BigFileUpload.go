package BigFileUpload

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
)

const (
	maxFileSize = 1 << 30 // 1GB
	chunkSize   = 1 << 26 // 64MB
)

func BigFileUpload() {
	http.HandleFunc("/upload", upload)
	http.HandleFunc("/uploadComplete", uploadCompleteHandler)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		//panic(err)
		fmt.Printf("Error starting server: %v\n", err)
	}
}

// 处理每个分块的上传请求，并将分块数据存储到临时目录中。
func upload(w http.ResponseWriter, r *http.Request) {

	file, header, err := r.FormFile("file") // 根据 file 字段获取上传的文件 => `multipart.File`
	if err != nil {
		fmt.Printf("err 34: %v \n", err)
		return
	}

	// q: 为什么要用defer关闭文件?
	// a: 因为在函数结束时，defer语句会按照逆序执行，所以在函数结束时，defer语句会关闭文件。
	defer file.Close() // 关闭文件

	// Check file size
	if header.Size > maxFileSize {
		http.Error(w, "File too large", http.StatusRequestEntityTooLarge)
		fmt.Printf("err 45: %v \n", err)
		return
	}

	// Create directory to store chunks
	fileID := r.FormValue("id")
	chunkDir := filepath.Join(".", "chunks", fileID)
	if err = os.MkdirAll(chunkDir, os.ModePerm); err != nil { // 创建多级目录，如果目录已经存在，则不会报错。
		http.Error(w, err.Error(), http.StatusInternalServerError)
		fmt.Printf("err 54: %v \n", err)
		return
	}

	// Determine chunk number
	chunkNum, err := strconv.Atoi(r.FormValue("chunk"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		fmt.Printf("err 62: %v \n", err)
		return
	}

	// Create chunk file
	chunkPath := filepath.Join(chunkDir, strconv.Itoa(chunkNum))
	out, err := os.Create(chunkPath) // 打开文件保存上传的文件
	if err != nil {
		fmt.Printf("err 62: %v \n", err)
		return
	}
	defer out.Close()

	// q: io.Copy() 有什么用？
	// a: io.Copy() 会将 file 中的数据拷贝到 out 中。
	// 		这里的 file 是一个 multipart.File 类型，它实现了 io.Reader 接口，所以可以作为 io.Copy() 的第一个参数。
	// 		out 是一个 *os.File 类型，它实现了 io.Writer 接口，所以可以作为 io.Copy() 的第二个参数。
	_, err = io.Copy(out, file) // 会返回两个值，分别是写入的字节数和错误信息
	if err != nil {
		fmt.Printf("err 62: %v \n", err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func combineChunk(filePath string, fileID string, chunkCount int) error {
	// Create output file
	outFile, err := os.Create(filePath) // 创建文件保存合并后的文件
	if err != nil {
		return err
	}
	defer outFile.Close()

	// Write chunks data to file
	for i := 0; i < chunkCount; i++ {
		chunkPath := filepath.Join(".", "chunks", fileID, strconv.Itoa(i))
		chunkFile, err := os.Open(chunkPath)
		if err != nil {
			return err
		}
		defer chunkFile.Close()

		if _, err = io.Copy(outFile, chunkFile); err != nil {
			return nil
		}
	}
	return nil
}

func calculateHash(fileID string) (string, error) {
	hasher := md5.New()
	hashChan := make(chan []byte)

	// hash each chunk in a separate goroutine
	for i := 0; ; i++ {
		chunkPath := fmt.Sprintf("./chunks/%s/%d", fileID, i)
		_, err := os.Stat(chunkPath)
		if err != nil {
			// q: 这里为什么要判断 os.IsNotExist(err) ？
			// a: 因为 os.Stat() 会返回两种错误，一种是文件不存在，一种是其他错误。
			// 		如果是文件不存在的错误，说明所有的分块都已经处理完毕，可以退出循环。
			// 		如果是其他错误，说明出现了问题，需要返回错误。
			if os.IsNotExist(err) {
				break
			}
			return "", err
		}

		// 每个 Goroutine 打开相应的文件块并计算哈希值。计算完成后，哈希值被写入到 Channel 中。
		go func(filePath string) {
			chunk, err := os.Open(filePath)
			if err != nil {
				hashChan <- []byte{}
				return
			}
			defer chunk.Close()

			_, err = io.Copy(hasher, chunk)
			if err != nil {
				hashChan <- []byte{}
				return
			}

			hashChan <- hasher.Sum(nil)
		}(chunkPath)

	}
	for i := 0; i < cap(hashChan); i++ {
		hash := <-hashChan
		if len(hash) > 0 {
			hasher.Write(hash)
		}
	}
	// return hash as hex string
	return hex.EncodeToString(hasher.Sum(nil)), nil
}

// 处理所有分块上传完成的请求，将分块合并成一个文件并计算哈希值。返回哈希值作为响应。
func uploadCompleteHandler(w http.ResponseWriter, r *http.Request) {
	fileID := r.FormValue("id")
	chunkCount, err := strconv.Atoi(r.FormValue("chunks"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		fmt.Printf("err 165: %v \n", err)
		return
	}

	// Combine chunks into single file
	filePath := filepath.Join(".", "files", fileID)
	if err = combineChunk(filePath, fileID, chunkCount); err != nil {
		fmt.Printf("err 165: %v \n", err)
		return
	}

	// Calculate MD5 hash
	hash, err := calculateHash(filePath)
	if err != nil {
		fmt.Printf("err 165: %v \n", err)
		//http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Return hash value to client
	w.Write([]byte(hash))
}
