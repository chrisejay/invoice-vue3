import { defineStore } from "pinia"
import { login } from "@/api/login"
// import store from ".."

interface UserState {
  token: string
  userInfo: object | null
  permissions: object | null
}

export const useUserStore = defineStore("user", {
  state: (): UserState => ({
    token: "default token",
    userInfo: { name: "wuhoooop!" },
    permissions: { a: 1 }
  }),
  getters: {
    getToken(): string {
      return this.token
    }
  },
  actions: {
    setToken(token: string) {
      this.token = token ?? ""
      const ex = 7 * 24 * 60 * 60 * 1000
    },
    resetToken() {
      this.userInfo = null
    },
    async login(params: API.LoginParams) {
      try {
        const { data } = await login(params)
        this.setToken(data.token)
      } catch (error) {
        return Promise.reject(error)
      }
    }
  },
  persist: {
    // piniaPluginPersist
    enabled: true,
    strategies: [
      {
        key: "__userinfo",
        storage: localStorage,
        paths: ["token", "userInfo"] // just persist "token" && "userInfo"
      }
    ]
  }
})

export function useUserStoreFunc(store?: any) {
  return useUserStore(store)
}
