import type { PropType } from "vue"

export interface filterOptionsType {
  label: string
  prop: string
  type: string
  placeholder: string
  options?: []
  selectOption?: string
  valueLabel?: string
  valueKey?: string
}
type placement =
  | "top"
  | "top-start"
  | "top-end"
  | "bottom"
  | "bottom-start"
  | "bottom-end"
  | "left"
  | "left-start"
  | "left-end"
  | "right"
  | "right-start"
  | "right-end"
interface tooltipTypes {
  tooltip:any
  placement: placement
  effect: "dark" | "light"
  content: string
  rawContent: boolean // 是否在content传入HTML 但容易受到XSS攻击
  showAfter: number
}

const props = {
  // 过滤条件
  filters: {
    type: Object as PropType<{}>,
    required: true,
    default: () => ({})
  },
  // 过滤按钮文字
  filterButtonText: {
    type: String as PropType<string>,
    default: "查询"
  },
  // 过滤配置
  filterOptions: {
    type: Array as PropType<(filterOptionsType & tooltipTypes)[]>,
    default: () => []
  },
  // 过滤的选择器的下拉选项
  pulldown: {
    type: Array as PropType<object[]>,
    default: () => []
  }
}

export default props
