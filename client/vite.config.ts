import { defineConfig } from "vite"
import vue from "@vitejs/plugin-vue"
import path from "path"
import Compression from "vite-plugin-compression" // gzip文件压缩
import AutoImport from "unplugin-auto-import/vite"
import Components from "unplugin-vue-components/vite"
import { ElementPlusResolver } from "unplugin-vue-components/resolvers"

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    assetsDir: "assets", // 打包后的静态资源目录
    rollupOptions: {
      output: {
        chunkFileNames: "assets/js/[name]-[hash].js", // 代码分割后的文件名
        entryFileNames: "assets/js/[name].[hash].js", // 入口文件名
        sourcemap: false, // 关闭sourcemap  
        // 静态资源文件名
        assetFileNames: chunkInfo => {
          // 用后缀名称进行区别处理
          // 处理其他资源文件名 e.g. css png 等
          const fileExt = path.extname(chunkInfo.name)
          const modelFile = // 这里没什么用,只是为了区分一下
            fileExt === ".gltf" || fileExt === ".glb" || fileExt === ".obj" || fileExt === ".fob"
          let subDir = "images"

          if (fileExt === ".css") {
            subDir = "css"
          } else if (modelFile) {
            subDir = "model"
          }
          return `assets/${subDir}/[name].[hash].[ext]` // 生成的文件名
        }
      }
    },
    minify: true, // 开启压缩(压缩后的代码将会去除注释、空格和其他不必要的字符，以减小文件大小。)
    terserOptions: {
      compress: {
        drop_console: true, // 去掉console
        drop_debugger: true // 去掉debugger
      }
    },
    // reportCompressedSize: false, // 关闭打包后的文件大小提示(压缩大型输出文件可能会很慢，因此禁用该功能可能会提高大型项目的构建性能。)
    cssCodeSplit: true, // 开启css分离
    assetsInlineLimit: 1024 * 5 // 小于 5kb 的图片会被转成base64
  },
  plugins: [
    vue(),
    // 自动导入
    AutoImport({
      resolvers: [ElementPlusResolver()] // 按需导入
    }),
    Components({
      resolvers: [ElementPlusResolver()] // 按需导入
    }),
    Compression({ threshold: 1024 * 512 }) // gzip : over 512kb
  ],
  server: {
    // proxy: {
    //   "/api": {
    //     target: "http://116.62.214.157:8080/",
    //     changeOrigin: true,
    //     rewrite: path => path.replace(/^\/api/, "")
    //   }
    // },
    port: 8080,
    open:true
  },
  resolve: {
    alias: [{ find: "@", replacement: "/src" }]
  }
})
