import { createPinia } from "pinia"
import type { App } from "vue"
import piniaPluginPersist from "pinia-plugin-persist"

const store = createPinia()
store.use(piniaPluginPersist)

export function setupStore(app: App<Element>) {
  app.use(store)
}

export default store
